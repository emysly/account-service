package com.reloadly.accountservice.repository;

import com.reloadly.accountservice.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByPhoneNumber(String phoneNumber);

    User findByEmail(String email);

    User findByUserName(String userName);

    User findByClientId(String clientId);

    boolean existsByPhoneNumber(String phoneNumber);

    boolean existsByEmail(String email);
}
