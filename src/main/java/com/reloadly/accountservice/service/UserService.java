package com.reloadly.accountservice.service;


import com.reloadly.accountservice.http.ApiResponse;
import com.reloadly.accountservice.http.UserCreateRequest;
import com.reloadly.accountservice.http.UserUpdateRequest;

public interface UserService {
    ApiResponse userCreate(UserCreateRequest userCreateRequest);
    ApiResponse userUpdate(UserUpdateRequest userUpdateRequest);
}
