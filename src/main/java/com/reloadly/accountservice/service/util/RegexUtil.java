package com.reloadly.accountservice.service.util;

public class RegexUtil {
    public static final String PHONE_NUMBER_REGEX = "^[+]*[(]?[0-9]{1,4}[)]?[-\\s/0-9]*$";
    public static final String PHONE_NUMBER_REGEX_VALIDATION_MESSAGE = "Phone number is invalid";
    public static final String PASSWORD_REGEX = "^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+*!=]).*$";
    public static final String PASSWORD_REGEX_VALIDATION_MESSAGE = "Password should have at least 1 uppercase, 1 lowercase, 1 special character and of 8 characters or more";
    public static final String EMAIL_REGEX = "^\\S+@\\S+\\.\\S+$";
    public static final String EMAIL_REGEX_VALIDATION_MESSAGE = "Email address is invalid";
}
