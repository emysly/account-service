package com.reloadly.accountservice.service.util;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Component;

@Component
public class UserHelper {

    public String generateClientId() {
        return RandomStringUtils.randomNumeric(8);
    }
}
