package com.reloadly.accountservice.service.impl;

import com.reloadly.accountservice.api.NotificationApi;
import com.reloadly.accountservice.exception.ResourceNotFoundException;
import com.reloadly.accountservice.exception.UserExistsException;
import com.reloadly.accountservice.http.*;
import com.reloadly.accountservice.model.User;
import com.reloadly.accountservice.repository.UserRepository;
import com.reloadly.accountservice.service.UserService;
import com.reloadly.accountservice.service.util.UserHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserHelper userHelper;
    private final NotificationApi notificationApi;

    @Override
    public ApiResponse userCreate(UserCreateRequest userCreateRequest) {

        if (userRepository.existsByPhoneNumber(userCreateRequest.getPhoneNumber())) {
            log.error("Phone number already exists: {}", userCreateRequest.getPhoneNumber());
            throw new UserExistsException("Phone number already exists");
        }

        if (userRepository.existsByEmail(userCreateRequest.getEmail())) {
            log.error("Email address already exists: {}", userCreateRequest.getEmail());
            throw new UserExistsException("Email address already exists");
        }


        User user = User.builder()
                .email(userCreateRequest.getEmail())
                .phoneNumber(userCreateRequest.getPhoneNumber())
                .clientId(userHelper.generateClientId())
                .password(passwordEncoder.encode(userCreateRequest.getPassword())).build();
        userRepository.save(user);

        log.info("User created...");

        EmailRequest emailRequest = EmailRequest.builder().to(user.getEmail()).subject("Account Registration")
                .text("Your Account with clientId " + user.getClientId() + " is registered successfully").build();

        notificationApi.sendEmail(emailRequest);

        UserCreateResponse userCreateResponse = UserCreateResponse.mapUserToUserCreateResponse(user);

        return ApiResponse.builder().success(true).status(201).message("User created successfully!").data(userCreateResponse).build();
    }

    @Override
    public ApiResponse userUpdate(UserUpdateRequest userUpdateRequest) {

        User user = userRepository.findByClientId(userUpdateRequest.getClientId());
        if (user == null) {
            log.error("User with clientId: {}, not found", userUpdateRequest.getClientId());
            throw new ResourceNotFoundException("User Not Found");
        }
        user.setUserName(userUpdateRequest.getUserName());
        user.setFirstName(userUpdateRequest.getFirstName());
        user.setLastName(userUpdateRequest.getLastName());

        log.info("Updated user with clientId: {}", user.getClientId());

        UserUpdateResponse userUpdateResponse = UserUpdateResponse.mapUserToUserUpdateResponse(user);

        return ApiResponse.builder().message("User updated successfully").success(true).status(200).data(userUpdateResponse).build();
    }

}
