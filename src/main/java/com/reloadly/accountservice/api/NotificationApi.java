package com.reloadly.accountservice.api;


import com.reloadly.accountservice.http.EmailRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@FeignClient(url = "${notification.service.base.url}", value = "notification-service")
public interface NotificationApi {
    @PostMapping("/api/v1/notification/email/send")
    String sendEmail(@Valid @RequestBody EmailRequest emailRequest);
}
