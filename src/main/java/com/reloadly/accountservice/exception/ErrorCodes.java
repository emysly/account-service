package com.reloadly.accountservice.exception;

public class ErrorCodes {
    //User Errors
    public static final String USER_EXISTS = "USER_ALREADY_EXISTS";

    //Field Errors
    public static final String INVALID_FIELD = "INVALID_FIELD";}
