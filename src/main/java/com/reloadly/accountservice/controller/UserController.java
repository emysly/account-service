package com.reloadly.accountservice.controller;

import com.reloadly.accountservice.http.ApiResponse;
import com.reloadly.accountservice.http.UserCreateRequest;
import com.reloadly.accountservice.http.UserUpdateRequest;
import com.reloadly.accountservice.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping("/create")
    public ApiResponse userCreate(@Valid @RequestBody UserCreateRequest userCreateRequest) {
        return userService.userCreate(userCreateRequest);
    }

    @PutMapping("/update")
    public ApiResponse userUpdate(@Valid @RequestBody UserUpdateRequest userUpdateRequest) {
        return userService.userUpdate(userUpdateRequest);
    }
}
