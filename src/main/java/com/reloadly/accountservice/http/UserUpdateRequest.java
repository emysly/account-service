package com.reloadly.accountservice.http;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
public class UserUpdateRequest {
    @NotNull(message = "ClientId is required.")
    private String clientId;
    private String firstName;
    private String lastName;
    private String userName;
}
