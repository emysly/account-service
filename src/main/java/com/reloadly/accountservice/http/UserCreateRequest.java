package com.reloadly.accountservice.http;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import static com.reloadly.accountservice.service.util.RegexUtil.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
public class UserCreateRequest {
    @NotEmpty
    @Pattern(regexp = PHONE_NUMBER_REGEX,
            message = PHONE_NUMBER_REGEX_VALIDATION_MESSAGE)
    private String phoneNumber;
    @NotEmpty
    @Pattern(regexp = PASSWORD_REGEX,
            message = PASSWORD_REGEX_VALIDATION_MESSAGE)
    private String password;
    @NotEmpty
    @Email(regexp = EMAIL_REGEX,
            message = EMAIL_REGEX_VALIDATION_MESSAGE)
    private String email;

}
