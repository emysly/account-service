package com.reloadly.accountservice.http;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.reloadly.accountservice.model.User;
import lombok.Builder;
import lombok.Data;


@Builder
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserUpdateResponse {
    private String clientId;
    private String email;
    private String phoneNumber;
    private String firstName;
    private String lastName;
    private String userName;

    public static UserUpdateResponse mapUserToUserUpdateResponse(User user) {
        return UserUpdateResponse.builder()
                .email(user.getEmail())
                .phoneNumber(user.getPhoneNumber())
                .userName(user.getUserName())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .build();
    }
}
