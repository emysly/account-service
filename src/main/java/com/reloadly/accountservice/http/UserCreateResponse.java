package com.reloadly.accountservice.http;

import com.reloadly.accountservice.model.User;
import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class UserCreateResponse {
    private String clientId;
    private String email;
    private String phoneNumber;

    public static UserCreateResponse mapUserToUserCreateResponse(User user) {
        return UserCreateResponse.builder()
                .clientId(user.getClientId())
                .email(user.getEmail())
                .phoneNumber(user.getPhoneNumber())
                .build();
    }
}
