package com.reloadly.accountservice.service;

import com.reloadly.accountservice.exception.ResourceNotFoundException;
import com.reloadly.accountservice.exception.UserExistsException;
import com.reloadly.accountservice.http.ApiResponse;
import com.reloadly.accountservice.http.UserCreateRequest;
import com.reloadly.accountservice.http.UserUpdateRequest;
import com.reloadly.accountservice.model.User;
import com.reloadly.accountservice.repository.UserRepository;
import com.reloadly.accountservice.service.impl.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private UserServiceImpl instance;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    public void whenUserPhoneNumberExistWhenCreatingAnAccount_thenReturnUnSuccessful() {

        UserExistsException exception = assertThrows(UserExistsException.class, () -> {
            when(userRepository.existsByPhoneNumber(anyString())).thenReturn(true);

            UserCreateRequest userCreateRequest = new UserCreateRequest("00000000000", "666666",
                    "test@gmail.com");

            instance.userCreate(userCreateRequest);
        });

        assertEquals(400, exception.getStatus());
        assertEquals("Phone number already exists", exception.getMessage());
        assertEquals("USER_ALREADY_EXISTS", exception.getErrorCode());

    }

    @Test
    public void whenUserEmailExistWhenCreatingAnAccount_thenReturnUnSuccessful() {

        UserExistsException exception = assertThrows(UserExistsException.class, () -> {
            when(userRepository.existsByEmail(anyString())).thenReturn(true);

            UserCreateRequest userCreateRequest = new UserCreateRequest("00000000000", "666666",
                    "test@gmail.com");

            instance.userCreate(userCreateRequest);
        });

        assertEquals(400, exception.getStatus());
        assertEquals("Email address already exists", exception.getMessage());
        assertEquals("USER_ALREADY_EXISTS", exception.getErrorCode());

    }

    @Test
    public void whenUserSuccessfullyCreatesAccount_thenReturnSuccessful() {

        when(userRepository.existsByPhoneNumber(anyString())).thenReturn(false);
        when(userRepository.existsByEmail(anyString())).thenReturn(false);
        when(passwordEncoder.encode(anyString())).thenReturn("password");

        UserCreateRequest userCreateRequest = new UserCreateRequest("00000000000", "666666",
                "test@gmail.com");

        ApiResponse response = instance.userCreate(userCreateRequest);

        assertTrue(response.isSuccess());
        assertEquals(201, response.getStatus());
        assertEquals("User created successfully!", response.getMessage());
        assertNotNull(response.getData());
        assertNull(response.getErrorCode());

    }

    @Test
    public void whenUserIsNotFoundWhenUpdatingAnAccount_thenReturnUnSuccessful() {

        ResourceNotFoundException exception = assertThrows(ResourceNotFoundException.class, () -> {
            when(userRepository.findByClientId(anyString())).thenReturn(null);

            UserUpdateRequest userCreateRequest = new UserUpdateRequest("00000000000", "test",
                    "test", "test");

            instance.userUpdate(userCreateRequest);
        });

        assertEquals(404, exception.getStatus());
        assertEquals("User Not Found", exception.getMessage());
        assertEquals("NOT_FOUND", exception.getErrorCode());

    }

    @Test
    public void whenUserIsFoundWhenUpdatingAnAccount_thenReturnSuccessful() {

        User user = new User("00000000000", "00000000000", "password",
                "test@gmail.com", "test", "test", "test");

            when(userRepository.findByClientId(anyString())).thenReturn(user);

            UserUpdateRequest userCreateRequest = new UserUpdateRequest("00000000000", "test",
                    "test", "test");

        ApiResponse response = instance.userUpdate(userCreateRequest);

        assertTrue(response.isSuccess());
        assertEquals(200, response.getStatus());
        assertEquals("User updated successfully", response.getMessage());
        assertNotNull(response.getData());
        assertNull(response.getErrorCode());

    }
}