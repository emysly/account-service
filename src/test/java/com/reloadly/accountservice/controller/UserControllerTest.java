package com.reloadly.accountservice.controller;

import com.reloadly.accountservice.http.*;
import com.reloadly.accountservice.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;


class UserControllerTest {

    @Mock
    private UserService userService;

    @InjectMocks
    private UserController instance;

    @BeforeEach
    public void setUp() {
        initMocks(this);
    }


    @Test
    public void whenUserRegistersAccountSuccessfully_thenReturnSuccessfulResponse() {

        UserCreateResponse userCreateResponse = UserCreateResponse.builder()
                .clientId("12281219821").email("test@gmail.com").phoneNumber("09088888888").build();

        ApiResponse apiResponse = ApiResponse.builder()
                .status(201).message("User created successfully!").success(true).data(userCreateResponse).build();

        when(userService.userCreate(any())).thenReturn(apiResponse);

        UserCreateRequest userCreateRequest = new UserCreateRequest("09088888888", "666666",
                "test@gmail.com");

        ApiResponse response= instance.userCreate(userCreateRequest);

        assertTrue(response.isSuccess());
        assertEquals(201, response.getStatus());
        assertEquals("User created successfully!", response.getMessage());
        assertNotNull(response.getData());
        assertNull(response.getErrorCode());

    }

    @Test
    public void whenUserUpdatesAccountSuccessfully_thenReturnSuccessfulResponse() {

        UserUpdateResponse userUpdateResponse = UserUpdateResponse.builder()
                .clientId("12281219821").firstName("test").lastName("test").userName("test").build();

        ApiResponse apiResponse = ApiResponse.builder()
                .status(200).message("User updated successfully").success(true).data(userUpdateResponse).build();

        when(userService.userUpdate(any())).thenReturn(apiResponse);

        UserUpdateRequest userUpdateRequest = new UserUpdateRequest("12281219821", "test",
                "test", "test");

        ApiResponse response= instance.userUpdate(userUpdateRequest);

        assertTrue(response.isSuccess());
        assertEquals(200, response.getStatus());
        assertEquals("User updated successfully", response.getMessage());
        assertNull(response.getErrorCode());
        assertNotNull(response.getData());

    }

}